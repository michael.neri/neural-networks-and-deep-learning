\documentclass[11pt, twocolumn]{article}

\RequirePackage[english]{babel} % lingua italiana
\RequirePackage{times} % carattere times
\RequirePackage{tikz} % pacchetto per fare grafici e disegni
\RequirePackage{url} % Pacchetto per l'inserimento di url 
\RequirePackage{amsmath,amssymb}  % Pacchetto per l'inserimento delle formule matematiche
\usepackage{bbm}
%%%%%%%%%%% Pacchetto che permette l'inserimento di codice Matlab formattato %%%%%
\usepackage{color} % Pacchetto per la gestione del testo colorato
\usepackage{listings} % inserisce listati di programmi
\usepackage{hyperref}
\usepackage{subfig}
%\usepackage[demo]{graphicx}
\definecolor{commenti}{rgb}{0.13,0.55,0.13}
\definecolor{stringhe}{rgb}{0.63,0.125,0.94}
\lstloadlanguages{Matlab}

\lstset{% general command to set parameter(s)
framexleftmargin=4mm,
frame=single,
keywordstyle = \color{blue},% blue keywords
identifierstyle =, % nothing happens
commentstyle = \color{commenti}, % comments
stringstyle = \ttfamily \color{stringhe}, % typewriter type for strings
showstringspaces = false, % no special string spaces
emph = {for, if, then, else, end},
emphstyle = \color{blue},
firstnumber = 1, % numero della prima linea
numbers =left, %  show number_line
numberstyle = \tiny, % style of number_line
stepnumber = 5, % one number_line after stepnumber
numbersep = 5pt,
language = {Matlab}, % per riconoscere la sintassi matlab
extendedchars = true, % per abilitare caratteri particolari
breaklines = true, % per mandare a capo le righe troppo lunghe
breakautoindent = true, % indenta le righe spezzate
breakindent = 30pt, % indenta le righe di 30pt
}

%%% equations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\e}[1]{%
	\ifmmode\refstepcounter{equation}%
	  \eqno\mbox{\rm(\theequation)}\label{e:#1}%
	\else(\ref{e:#1})\fi}
	
\DeclareMathOperator{\E}{\mathbb{E}}

%%% eqalign %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\catcode`@=11
\newdimen\jot \jot=3pt
\def\openup{\afterassignment\@penup\dimen@=}
\def\@penup{\advance\lineskip\dimen@
  \advance\baselineskip\dimen@
  \advance\lineskiplimit\dimen@}
\def\eqalign#1{\null\,\vcenter{\openup\jot\m@th
  \ialign{\strut\hfil$\displaystyle{##}$&$\displaystyle{{}##}$\hfil
      \crcr#1\crcr}}\,}
      
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%% TITOLO %%%%%%%%%%%%%%%%%%%%%

\title{%
  Homework 3 Neural Networks And Deep Learning A.Y 2020/2021 \\
  \large Reinforcement Learning - CartPole-v1 and MountainCar-v0}
\date{Padova, \today{}}
\author{Michael Neri \\ ID : 1232224}
\maketitle


%%%%%%%%%%%%%%% TESTO %%%%%%%%%%%%%%%%%%%%%
\twocolumn
\section{Introduction}
\textbf{Reinforcement Learning} is a specific field in Artificial Intelligence in order to recreate animal and human behaviour. Supervised and Unsupervised tasks are performed by inspecting correlations in the input space of the model whereas Reinforcement Learning studies how an agent takes action into an environment in order to maximize an objective function. Therefore, a Reinforcement Learning structure is based on two main parts: \textbf{Agent} and \textbf{Environments}.


At each step $t$ the \textbf{Agent} receives 
\begin{itemize}
\item $s_t$ State of the environment at time $t$;
\item $r_t$ Reward obtained from the environment;
\end{itemize}
and then it executes the action at time $t$ $a_t$.
The \textbf{Environment} instead receives the action from the agent $a_t$ and outputs $r_t$ and $s_t$.

Agent chooses its actions w.r.t.  a \textit{Policy} function $\pi$ given a certain state so it yields
\begin{equation}
a = \pi (s)
\end{equation}
We can see that our policy function $\pi$ is a fundamental component of the Reinforcement Learning algorithm since it can model player behaviour and permits to the agent to learn how to obtain feedbacks from past actions/rewards tuples.
We can formally define $q_{\pi}$ as the action-value function in order to understand how good the move is w.r.t. that moment following policy $\pi$. 
Its formula can be written as 
\begin{equation}
Q_t^{\pi} (s_t,a_t)  = \E [r_{t+1} + \gamma r_{t+2} + \gamma^2 r_{t+3} . . .  | a_t, s_t] 
\end{equation}
where $\gamma$ is called the \textit{discount factor} which takes into account long-term rewards.  It must be a value between $0$ and $1$ for convergence of the sum.
Previous equations is also called as the \textbf{q-value} of a certain action w.r.t. a specific state. (where the $q$ stands for "Quality" of the action).  Main aim of RL algorithm is to find a specific policy which permits to the player to obtain the maximum cumulative reward hence finding the optimal policy
\begin{equation}
Q^{*} (s_t,a_t) = \max_{\pi} Q_t^{\pi} (s_t,a_t)
\end{equation}
Thanks to DQN models (\textit{Deep Q-Network}), we are able to approximate optimal policy $Q^{*}$ using weights $\mathbf{\theta}$ of a neural network by feeding $s_t$ the state of the environment at time $t$  and obtaining the corresponding off-policy action $a_t$. In order to apply Gradient Descent algorithms, we have to define our loss function (or objective function) which is related to the \textbf{Bellman equation} for the Q-function:
\begin{equation}
L(\mathbf{\theta} ) = \E [ \underbrace{(r_t + \gamma \max_{a_{t}'} Q(s_{t}',  a_{t}' ,  \mathbf{\theta} )}_{\mathrm{target}} - \underbrace{Q(s_t,a_t, \mathbf{\theta})}_{\mathrm{prediction}})^2 ]
\end{equation}

Since the target equation is nonstationary, we implement two deep networks in the code
\begin{itemize}
\item \textit{prediction network} which is trained at each step;
\item \textit{target network} which is periodically updated used for action selection.
\end{itemize} 
We implement also a ReplayMemory which permits to store previous learning episodes with tuples of $(s_t,a_t,r_{t+1},s_{t+1})$ in order to increase stability of Q-values learning.  

Following sections will explain how different exploration profiles are used in order to model player behaviour, how much important hyperparameters are in a RL enviroment using Optuna, use a CNN (\textit{Convolutional Neural Network} in order to learn the optimal policy from pixels of Gym CartPole-V1 environment and then inspect a new Gym environment (MountainCar-V0) with its solution.

\section{Methods}
\subsection*{Exploration profile - Modelling type of playing}
Thanks to the exploration profile, we are able to model how our player is going to behave with the environment. In CartPole-v1 using discrete states and pixels we are going to use a decreasing exponential which can be seen in Fig.\ref{Fig:expexp}.

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/expexploration}
\caption{Decreasing Exploration Profile.}
\label{Fig:expexp}
\end{figure}

This exploration profile permits to the agent not to be too greedy in order to avoid local maximum. Hence, some actions are chosen in order to explorate better combinations of steps in order to maximize our objective function.

We change the exploration profile in a mixed decreasing exponential and a cosine function for solving the MountainCar-V0 in order to simulate a player which
\begin{itemize}
\item Continues to find new possible moves in order to increase the overall score and reach the target;
\item Keeps small modifications in its playing style.
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/expsin}
\caption{Decreasing Exploration Profile with Cosine behaviour.}
\label{Fig:expcos}
\end{figure}

In Fig.\ref{Fig:expcos} we can inspect how we can model a different exploration profile.

\subsection*{Tuning Hyperparameters - Optuna and RL}
Thanks to Optuna module, we are able to tune hyperpatameters of the RL structure and understand which parameter is the most relevant in order to maximize the score (or win the game) with the least number of episode. 
We define the study by wrapping the train function which return the mean of all the episode and then we find the tuple of hyperparameters which performs best w.r.t. average score. 

We apply Optuna only for solving CartPole-V1 using states obtained by \textit{env.step()} function.

\begin{table}[h!]
\centering
\begin{tabular}{|l|l|}
\cline{1-2}
\textbf{Penalty Bad State}       & 6       \\ \cline{1-2}
\textbf{Batch Size}        & 256       \\ \cline{1-2}
\textbf{$\mathbf{\gamma}$ Discount Factor} & 0.9997975431228965   \\ \cline{1-2}
\textbf{Learning Rate}    & 0.00016393431203613848     \\ \cline{1-2}
\textbf{net-update-steps}    & 25   \\ \cline{1-2}
\textbf{Policy}    & Softmax    \\ \cline{1-2}
\end{tabular}
\caption{List of best hyperparameters for CartPole-V1.}
\label{Tab:1}
\end{table}


With $3.5$ hours of training and $75$ studies tested, we are able to define our set of hyperparameters with their importance in Tab.\ref{Tab:1} and in Fig.\ref{Fig:Optuna} where we can see that \textbf{Discount Factor} $\gamma$ is the most important parameter in the RL environment.

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.2]{figures/OptunaBestParams}
\caption{Importance of Hyperparameters in RL environment.}
\label{Fig:Optuna}
\end{figure}

\subsection*{CNN as Policy Network - 4 frames as state}
Instead of using the $4$ states provided by the Gym environment, we are going to train our Deep Q-Network by using pixels so feeding the network with screen of the game. 

Since understanding the physics of the pole is fundamental for controlling it, we need to store more than one frame at time in order to estimate velocity of the cart and the angular velocity of the pole and decide where to push.

For this reason a \textbf{StateContainer} object is defined, very similar to our ExperienceReplay object, which stores $4$ consecutive frames in order to recreate the movement of the CarPole.  We implement also GPU integration in order to speed up training computation.

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.5]{figures/ScoringCNN}
\caption{Score History using CNN DQN.}
\label{Fig:scorecurveCNN}
\end{figure}

This type of learning is very slow w.r.t. convergence so it needs a lot of episodes in order to create a good representation of the environment using Convolutional Networks.  We use Convolutional Networks in order to obtain the maximum amount of spatial information from the display. In our set up, training phase lasts $850$ episodes and its scoring curve is visible in Fig.\ref{Fig:scorecurveCNN}. This procedure took $6$ hours on Google Colab approximately using hyperparameters obtained by Optuna optimization in the previous section.

We can inspect some examples of playing in the .ipynb file using the command \textit{show\_videos()}.

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.4]{figures/StructureCNN}
\caption{Structure of DQN for elaborating pixels.}
\label{Fig:StructureCNN}
\end{figure}


In Fig.\ref{Fig:StructureCNN} we can see how our DQN is codified in order to choose actions w.r.t. gym's pixels obtained by the command \textit{env.render( mode = 'rgb\_array ') }. Main differences are:
\begin{itemize}
\item At the beginning we have to declare the presence of $4$-channels input since we stack the states into a single tensor;
\item Perform the flattening module in order to feed the result of the CNN part into a fully-connected section. Outputs of Linear part are the \textbf{q-values} $Q(s_t, a_t)$ w.r.t. a specific state and action at time $t$.
\end{itemize}

Unfortunately, our \textbf{agent} is not able to win the game but some interesting results are obtained in the test set since, for this type of learning, few hours of training are applied to our CNN DQN.

\subsection*{MountainCar-V0 Environment}
MountainCar-V0 is an environment which is provided by OpenAI Gym. The game consists of reaching a flag on the top of a mountain by a car. This car has to climb correctly the mountain and then reach the flag. It is not possibile to reach the goal by accelerating once so AI needs to understand that, in order to win, it has to take a run up from initial position.
In Fig.\ref{Fig:MountainCargame} we can visualize a screen of a initial state of the game.

Environment can be summarized by describing its action and state space:
\begin{itemize}
\item \textbf{State} space consists of two continuous values which describe position and velocity of the car;
\item \textbf{Action} space consists of three possibile movements which are $\{0,1,2\}$ i.e. no movements, acceleration on the right or on the left.
\end{itemize}

Our DQN then will be a fully connected network with $2$ input and $3$ values which are the possible \textbf{q-values}.  Even if our policy network and training loop is correctly coded, we have to help our player in order to speed up computation. 

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.5]{figures/MountainCargame}
\caption{MountainCar-V0 Environment.}
\label{Fig:MountainCargame}
\end{figure}

 
\subsection*{Solution of MountainCar-V0 Gym Environment}
If no suggestions are given to the Agent then car starts oscillating from initial position to the flag without reaching it. For this reason, we implement a reward by using the state provided by the gym and a bonus if it reaches the flag. Hence, we can summarize the bonus as following:
\begin{itemize}
\item With respect to the position, at each step we apply an additional reward which depends on the previous state because
\begin{equation}
reward = \beta  |x^{(t+1)} - x^{(t)}  |  
\end{equation}
where $x^t$ denotes the position at time $t$ and $\beta$ is a real coefficient which can be used as a hyperparameter of the training loop;
\item We add $200$ point if the car reaches the flag which is at position $x^{\mathrm{goal}} = 0.5$.
\end{itemize}
 Thanks to these suggestions, we are able to win the game in $1000$ episodes.
 
 Better results can be obtained if we fine-tune our hyperparameters using Optuna as we have already seen in the first Section of this document.
\begin{figure}[h!]
\centering
\includegraphics[scale = 0.4]{figures/ScoreTrainingMountain}
\caption{Score History fot MountainCar-V0 environment.}
\label{Fig:ScoreTrainingMountain}
\end{figure}

\subsection*{Videos}
In the folder of the HW, there are also some videos from test phase.
\begin{itemize}
\item \textit{OptunaCartPole} shows how our cartpole wins the game by nullifying angular velocity;
\item \textit{CNNCartPole} shows how the agent controls the cartpole by using only pixels of the screen;
\item \textit{WinMountainCar} shows how the car reaches fastly the goal by taking a run-up at the beginning. 
\end{itemize}

We included videos in the folder since all the work was implemented in the Google Colab environments and no videos are present in the .ipynb file.
\section*{Appendix}
\onecolumn

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/SlicePlotOptuna}
\caption{Slice Plot from Optuna.}
\label{Fig:OptunaSlice}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/StudyHistory}
\caption{Study History from Optuna}
\label{Fig:OptunaHistory}
\end{figure}



%%%%%%%%%%%%%%% GUIDA %%%%%%%%%%%%%%%%%%%%%

%\section{Codice \textsc{Matlab}}

%\lstinputlisting{Tesina2018SS.m}


\end{document}


\endinput
