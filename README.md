# Neural Networks and Deep Learning

Folder of all homeworks for Neural Networks and Deep Learning @Unipd A.A. 20/21 using Pytorch

- HW1 - Supervised Learning - Regression and Classification
    - Recreate an unknown function by using a fully connected network. 
    - Use a CNN for MNIST written digits dataset classification.
    - Implementing a naive GridSearch and Pseudo Random Search.
- HW2 - Unsupervised Learning - AutoEncoders and GAN
    - Employing AutoEncoders for compression and denoising purposes. 
    - Implementation of a MNIST written digits GAN.
- HW3 - Reinforcement Learning - Optuna and OpenGym Environment
    - Use states of Gym environment.
    - Using pixels of the screen in order to train the network.
    - Use of Optuna for optimization of hyperparameters.
    - Winning CartPole-v1 and MountainCar-V0 using decreasing exponential exploration profile and customized exploration profile.



