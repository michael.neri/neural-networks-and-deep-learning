\documentclass[10pt, twocolumn]{article}

\RequirePackage[english]{babel} % lingua italiana
\RequirePackage{times} % carattere times
\RequirePackage{tikz} % pacchetto per fare grafici e disegni
\RequirePackage{url} % Pacchetto per l'inserimento di url 
\RequirePackage{amsmath,amssymb}  % Pacchetto per l'inserimento delle formule matematiche
\usepackage{bbm}
%%%%%%%%%%% Pacchetto che permette l'inserimento di codice Matlab formattato %%%%%
\usepackage{color} % Pacchetto per la gestione del testo colorato
\usepackage{listings} % inserisce listati di programmi
\usepackage{hyperref}
\definecolor{commenti}{rgb}{0.13,0.55,0.13}
\definecolor{stringhe}{rgb}{0.63,0.125,0.94}
\lstloadlanguages{Matlab}

\lstset{% general command to set parameter(s)
framexleftmargin=4mm,
frame=single,
keywordstyle = \color{blue},% blue keywords
identifierstyle =, % nothing happens
commentstyle = \color{commenti}, % comments
stringstyle = \ttfamily \color{stringhe}, % typewriter type for strings
showstringspaces = false, % no special string spaces
emph = {for, if, then, else, end},
emphstyle = \color{blue},
firstnumber = 1, % numero della prima linea
numbers =left, %  show number_line
numberstyle = \tiny, % style of number_line
stepnumber = 5, % one number_line after stepnumber
numbersep = 5pt,
language = {Matlab}, % per riconoscere la sintassi matlab
extendedchars = true, % per abilitare caratteri particolari
breaklines = true, % per mandare a capo le righe troppo lunghe
breakautoindent = true, % indenta le righe spezzate
breakindent = 30pt, % indenta le righe di 30pt
}

%%% equations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\e}[1]{%
	\ifmmode\refstepcounter{equation}%
	  \eqno\mbox{\rm(\theequation)}\label{e:#1}%
	\else(\ref{e:#1})\fi}

%%% eqalign %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\catcode`@=11
\newdimen\jot \jot=3pt
\def\openup{\afterassignment\@penup\dimen@=}
\def\@penup{\advance\lineskip\dimen@
  \advance\baselineskip\dimen@
  \advance\lineskiplimit\dimen@}
\def\eqalign#1{\null\,\vcenter{\openup\jot\m@th
  \ialign{\strut\hfil$\displaystyle{##}$&$\displaystyle{{}##}$\hfil
      \crcr#1\crcr}}\,}
      
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%% TITOLO %%%%%%%%%%%%%%%%%%%%%

\title{%
  Homework 1 Neural Networks And Deep Learning A.Y 2020/2021 \\
  \large Regression and Classification task}
\date{Padova, \today{}}
\author{Michael Neri \\ ID : 1232224}
\maketitle


%%%%%%%%%%%%%%% TESTO %%%%%%%%%%%%%%%%%%%%%
\twocolumn
\section{Introduction}
%% Part 1
A Neural Network (\textit{NN}), denoted as $\mathcal{G}$,  is defined as a set of simple units, called \textit{neurons} or \textit{perceptrons} which elaborate data using weights on their connections in order to detect correlation of data and process received information.  We call $\mathcal{E}$ the set of all weighted edges and $\mathcal{V}$ the sel of all the vertices (neurons) of the graph.   We can divide the neural network in three main parts:
\begin{itemize}
\item \textbf{Input Layer} where information is codified in order to be processed by the network. We call $N_0$ as the number of neurons in the input layer. The first neuron is not counted since it is defined using homogeneous coordinates;
\item \textbf{Hidden Layers} where data is elaborated using additions, multiplications and activation function $\sigma$ at the output of each single neuron;
\item \textbf{Output Layer} where processed data of the network is then converted in a form which depends on the learning problem.  We call $N_{T}$ as the number of neurons in the final layer where $T$ is the \textit{depth} of the neural network;
\end{itemize}
When we train a NN,  we are assigning weights to the edges so, in a mathematical form,  $w:\mathcal{E} \rightarrow \mathbb{R}$.
We can summarize the structure of the NN $\mathcal{G}$ using the following equation:
\begin{equation}
h_{E, V, \sigma, w} : \mathbb{R}^{N_0 -1} \rightarrow \mathbb{R}^{N_T}
\end{equation}
where $h$ is the function which encapsulates the behaviour of the network and belongs to an hypothesis space  $\mathcal{H}$ which contains all the possible solutions w.r.t. a specific learning problem. 

\textbf{Learning process} relies on two fundamental algorithms which are \textit{forward propagation} and \textit{back-ward propagation}. We elaborate an input, we compare the prediction of the input with the real value (in case of supervised learning) in order to obtain the error. This value is then used in order to adjust the values of the weighted connections $w$ and to reduce the error at the next observation of the data. Velocity and stability of learning process can be modified using coefficients like the  \textit{learning rate} and the \textit{weight decay}.  In this first homework we are going to focus on only \textit{supervised learning} task so data is labelled during training in order to correct wrong predictions.

\section{Part 1 - Regression}
The goal of the first part of the homework 1 is to approximate a function on the basis of its samples (\textit{regression task}). Performance of the neural network is then evaluated using a separated set of samples (test set) using the \textit{Mean Squared Error} comparison.   Hence, 
$$
\mathit{f}: \mathbb{R} \rightarrow \mathbb{R} \quad
x \rightarrow y = \mathit{f}(x) \quad
network(x) \approx \mathit{f}(x) 
$$
In Fig.\ref{TrainingSet}, we can see the training set for the regression task ($100$ samples).  For this homework,  we are going to use a fully-connected network where its hyperparameters are determined using Grid Search with Cross-Validation method.

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/trainingPoints}
\caption{Training set. }
\label{TrainingSet}
\end{figure}

In our implementation, together with MinMaxNormalization of the dataset,  we use Pytorch library for defining structure of our fully-connected neural network (or also called \textit{Multiple Layer Perceptron}) which consists of: 
\begin{itemize}
\item \textbf{Input layer} with input size $1$  and output size $N_{h1}$. Input size is unitary because our hypothesis space $\mathcal{H}$ is defined as the set of all hypothesis $h$ s.t.
$$
h_{E, V, \sigma, w} : \mathbb{R} \rightarrow \mathbb{R}
$$ In other words, we are recreating a 1D function from its training samples;
\item \textbf{Hidden Layer} with input size $N_{h1}$ and output size $N_{h2}$;
\item \textbf{Output Layer} with input size $N_{h2}$ and output size $1$ which is the prediction of the evaluation of the function $\mathit{f}$. At the end we apply an \textit{Sigmoid} activaction function $\sigma$.
\end{itemize}
Errors are estimated using the MSE metric which are used for the backward propagation algorithm.
Thanks to this implementation,  we can define a tuple (number of neurons, epochs, learning rate, batch size) of hyperparameters in order to find the best model for the given training set. No \textit{Dropout} (Bernoulli probability of losing connections during the learning process) and \textit{Weight Decay} (limitation of the weights magnitude or so called \textit{L2 regularization}) are used since few training points are available. 

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/BestValTra}
\caption{Loss of validation and training of best model according to Grid Search. }
\label{BestValTra}
\end{figure}

After performing Grid-Search with Cross-Validation, we have the values of validation and training loss of the architecture which has, as hyperparameters:
\begin{itemize}
\item $N_{h1} = N_{h2} = 512$ neurons. Only symmetric cases ($N_{h1} = N_{h2}$) are taken into account;
\item $5000$ epochs;
\item $0.01$ learning rate;
\item $100$ batch size (so the whole dataset is fed into the network).
\end{itemize}
From the plot in Fig.\ref{BestValTra}, we can deduct that we can stop training after $1000$ epochs since validation loss is not decreasing anymore (overfitting).

At the end of the learning process,  we freeze the weights of the network and plot the model of the function in Fig.\ref{BestModel} where also test points ($100$ points) are plotted in order to evaluate performance of the regression ($MSE \approx 0.14$ on test set). 

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/BestModel}
\caption{Model of the function in the range of the training set w.r.t. test points.}
\label{BestModel}
\end{figure}
Network output is not smooth in some regions (such as in the normalized interval $[0.2 0.4]$) since none of training point is present in that specific interval. For this reason, network tries to define a strange behaviour which is similar to an overfitting problem.  Using a low number of epochs and batch size, this specific reason is not a problem. Instead, in the interval $ [0.6,0.8]$, network model fails completely (concavity of the function is inverted) because of the same reason as previously mentioned (no training points).

\subsection{Tuning Hyperparameters - Grid Search}
Grid Search is a powerful method for finding good hyperparameters for our model and obtaining good results in the test set. For the regression task, we define a set of fixed hyperparameters and we create all the possible combinations. As we can imagine, this method is computationally demanding without using ad-hoc libraries such as OpTuna.
In this environment, we used \textit{ipertools.product}function in order to create tuples of hyperparameters. Weight decay and Dropout are not in the study because of the number of samples.  In Tab.\ref{Tab:1} we can inspect the values used in the Grid Search. In terms of computation time, the algorithm requestes approximately $1$ hour in order to select the best hyperparameters for this situation. This time depends on the size of the training set and on the number of possible combinations.

\begin{table}[]
\centering
\begin{tabular}{|l|l|}
\cline{1-2}
\textbf{Neurons}       & (256,512,1024)        \\ \cline{1-2}
\textbf{Epochs}        & (2000,4000,5000)       \\ \cline{1-2}
\textbf{Learning Rate} & (1e-2,1e-3,1e-4,1e-5)   \\ \cline{1-2}
\textbf{Batch Size}    & (80,100)     \\ \cline{1-2}
\end{tabular}
\caption{List of dicrete values of hyperparameters.}
\label{Tab:1}
\end{table}

\section{Part 2 - Classification}
%% Part 2
Second part of the homework is to perform a multiclassification task using MNIST dataset of written numbers. Even if our samples are images, a fully-connected network (using an input layer of $28\times 28$ neurons) can perform well but a Convolutional Neural Network (\textit{CNN}) is developed directly in order to obtain great values of accuracy in supervised learning.  

Our dataset is composed of $60,000$ training and $10,000$ test images of written numbers from MNIST dataset. During the training phase, an \textbf{Early Stopping} condition is applied when training accuracy reaches $100\%$. Thanks to this implementation, learning process is much less time demanding. 

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.5]{figures/BestValTraCla}
\caption{Model of the function in the range of the training set w.r.t. test points.}
\label{BestValTraCla}
\end{figure}

In Fig.\ref{BestValTraCla} we can see the loss of training and validation using best hyperparameters obtained from the \textit{Pseudo Random Search}.  One example is $(\mathrm{epoch,batch_size,learning rate}) = (98, 173, 0.000390625)$.  

Thanks to this configuration, we can perform predictions on MNIST written numbers dataset with an accuracy of $99.13\%$ on test set. Some example can be seen from Fig.\ref{example} and the test accuracy can be seen from the \textit{confusion matrix} in Fig.\ref{confusionMatrix}.

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/confusionMatrix}
\caption{Confusion Matrix.}
\label{confusionMatrix}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.4]{figures/try}
\caption{Images, GroundTruth and prediction for some examples.}
\label{example}
\end{figure}

\subsection{Use of CNN for classification task}
In our setup, the CNN architecture is as follow:
\begin{itemize}
\item First \textbf{Convolutional 2D} Layer with input channel $= 1$ (since is a gray image of MNIST) and output channel $= 16$ with kernel size $ = 5$, padding $ = 2$ and stride $= 1$;
\item Second \textbf{Convolutional 2D} Layer with same parameters but input channel $ = 16$ (since it has to elaborate first layer output), output channel $ = 32$;After each Convolutional Layer, we apply a \textbf{MaxPool2D} layer in order to reduce the dimensionality of learning parameters of the network;
\item Activation function $\sigma$ is the \textbf{ReLU} function which is $$ReLU(x) = max(0,x)$$
\item \textbf{Dropout} layer is added which removes connections randomly in order not to overfit the network;
\item Since we are performing a multiclassification task and we have $10$ classes, final layers are two \textbf{fully-connected} layers which converts all the output of the convolutional part and we apply a \textbf{Flatten} layer (all the values are inside a 1-D vector). Then first linear layer is composed of $100$ neurons and the last one of $10$ neurons. In this manner, network outputs which number is written on the image.
\end{itemize}

\subsection{Tuning Hyperparameters -Pseudo Random Search}
In the notebook, we implemented a 'Pseudo' Random Search using the Python library \textit{random}. The term 'Pseudo' means that no Probability Distribution Function (\textit{pdf}) is defined w.r.t. the results of accuracy obtained after the training loop. Hence, hyperparameters are chosen randomly (uniformly distributed) among a dense poll of possible values.  In this environment, we perform $40$ different training and, adding the \textit{Early Stopping} condition, we obtain an accetable result in terms of test accuracy and training time (about $30$ minutes in Google Colab).  We can inspect parameters of the Pseudo Random Search as follow: 

\begin{table}[]
\centering
\begin{tabular}{|l|l|}
\cline{1-2}
\textbf{Epochs} - \textit{int}        & [20,60]      \\ \cline{1-2}
\textbf{Learning Rate} - \textit{float} & $\frac{0.1}{2^j}\quad 0 \leq j \leq 10$  \\ \cline{1-2}
\textbf{Batch Size} - \textit{int}   & [50,600]     \\ \cline{1-2}
\end{tabular}
\caption{Parameters of Pseudo Random Search.}
\end{table}

  
\section{Appendix}
\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/NetworkModelWithTestSet}
\caption{Model of the function in the range of the training set w.r.t. test points.  Case with lower epochs (100) and batch size (8).}
\end{figure}
\begin{figure}[h!]
\centering
\includegraphics[scale = 0.2]{figures/histRegress}
\caption{Histograms of activation of regression task.}
\label{histRegress}
\end{figure}
\begin{figure}[h!]
\centering
\includegraphics[scale = 0.3]{figures/kernelConv}
\caption{Visualization of convolutional filters ($1^{st}$ layer).}
\label{kernelConv}
\end{figure}

\end{document}


\endinput
